﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/* Zrobił
 * Kamil Buczel
 * Adam Ławniczak
 * */

namespace ConsoleApplication4
{
    class Program
    {
        static int x = 10, y = 10;
        static bool[,] Plan;
        static bool[,] StaryPlan;
        static public bool checkifright(int a, int b)
        {
            if (a < 0 || a >= x) return false;
            if (b < 0 || b >= y) return false;
            if (StaryPlan[a, b]) return true;
            return false;
        }

        static public int sasiedzi(int x, int y)
        {
            int s = 0; if (checkifright(x - 1, y - 1)) s++;
            if (checkifright(x, y - 1)) s++; if (checkifright(x + 1, y - 1)) s++;
            if (checkifright(x - 1, y)) s++; if (checkifright(x + 1, y)) s++;
            if (checkifright(x - 1, y + 1)) s++; if (checkifright(x, y + 1)) s++;
            if (checkifright(x + 1, y + 1)) s++; return s;
        }

        static public void wyswietl()
        {
            Console.Clear();
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    if (Plan[i, j])
                        Console.Write('O');
                    else
                        Console.Write('X');
                }
                Console.Write("\n");
            }
        }

        static public void dupablada()
        {
            int s = 0;
            Plan = new bool[x, y];
            StaryPlan = new bool[x, y];
            /*StaryPlan[0, 3] = true; 
            StaryPlan[0, 4] = true; 
            StaryPlan[1, 3] = true; 
            StaryPlan[1, 4] = true;*/

            Random rnd = new Random(); // generowanie losowe
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    StaryPlan[i, j] = (rnd.Next(0, 2) == 1 ? true : false);
                }
            }


            while (true)
            {
                /*for (int i = 0; i < x; i++) // zamienic na CopyTo() 
                { 
                    for (int j = 0; j < y; j++) 
                    {
                        Plan[i,j] = StaryPlan[i,j]; } }*/

                Plan = (bool[,])StaryPlan.Clone();

                wyswietl();

                for (int i = 0; i < x; i++)
                {
                    for (int j = 0; j < y; j++)
                    {
                        s = sasiedzi(i, j);
                        if (s < 2 || s > 3)
                            Plan[i, j] = false;
                        else
                        {
                            if (s == 3)
                                Plan[i, j] = true;
                        }
                    }
                }

                /*for (int i = 0; i < x; i++) // zamienic na CopyTo() 
                {
                    for (int j = 0; j < y; j++)
                    {
                        StaryPlan[i, j] = Plan[i, j];
                    }
                }*/
                StaryPlan = (bool[,])Plan.Clone();


                Console.Read();
            }
        }
        static void Main(string[] args) { dupablada(); }
    }
}
